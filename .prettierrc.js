module.exports = {
  printWidth: 100,
  semi: false,
  singleQuote: true,
  trailingComma: 'none',
  useTabs: false,
  tabWidth: 2,
  quoteProps: 'as-needed',
  bracketSpacing: true,
  bracketSameLine: false,
  vueIndentScriptAndStyle: false
}
