# Mifiesta Swagger API

Включает в себя:

- контракты
- API-методы

### Правила оформления

Используется [OAS](https://swagger.io/specification/) и [кастомные дополнения](./RULES.md)
